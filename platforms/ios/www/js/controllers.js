angular.module('starter.controllers', [])

  .controller('AppCtrl', AppCtrl)                 // all
  .controller('AccountingCtrl', AccountingCtrl)   // employee
  .controller('AttendanceCtrl', AttendanceCtrl)   // employee
  .controller('BalanceCtrl', BalanceCtrl)         // manager
  .controller('ConnectionCtrl', ConnectionCtrl)   // employee
  .controller('HoursCtrl', HoursCtrl)             // manager->employee
  .controller('EmployeesCtrl', EmployeesCtrl)     // manager
  .controller('ExpensesCtrl', ExpensesCtrl)       // manager
  .controller('FlowCtrl', FlowCtrl)               // employee
  .controller('IncomeCtrl', IncomeCtrl)           // manager
  .controller('LocationCtrl', LocationCtrl)       // manager->employee
  .controller('LoginCtrl', LoginCtrl)             // all
  .controller('MapCtrl', MapCtrl)                 // all
  .controller('PermissionsCtrl', PermissionsCtrl) // manager
  .controller('ProfileCompanyCtrl', ProfileCompanyCtrl)       // manager
  .controller('ProfileEmployeeCtrl', ProfileEmployeeCtrl)     // employee
  .controller('ProfileForManagerCtrl', ProfileForManagerCtrl) // manager->employee
  .controller('RegisterCtrl', RegisterCtrl)                   // all
  .controller('RegisterAdditionalCtrl', RegisterAdditionalCtrl)       //all
  .controller('RegisterPointsCtrl', RegisterPointsCtrl)               // employee
  .controller('RequestsCtrl', RequestsCtrl)       // manager
  .controller('SalariesCtrl', SalariesCtrl)       // employee, manager->employee
  .controller('StatisticsCtrl', StatisticsCtrl)   // manager

;

function AppCtrl($rootScope, $localStorage, $scope, AuthService, $state) {

  $rootScope.company_id = $localStorage.company_id;

  $scope.logout = function () {

    AuthService.logout();
    $state.go('app.login');

  };

}


function AccountingCtrl(WebService, $ionicPopup, $http, $rootScope, $scope, AuthService, $state, $localStorage) {

  $scope.doRefresh = function () {

    $rootScope.getActiveEmployees($rootScope.company_id).finally(function () {
      $scope.$broadcast('scroll.refreshComplete');
    });

  };

  $scope.sendAccounting = sendAccounting;
  $scope.updateAccounting = updateAccounting;
  $scope.showLocation = showLocation;

  function sendAccounting() {

    $http.post(WebService.ADDACCOUNTING, {

      company_id: $localStorage.company_id,
      income: $rootScope.accounting.income,
      expenses: $rootScope.accounting.expenses,
      employee_id: AuthService.getUser(),
      date: moment().format('YYYY-MM-DD')

    }).then(successSendAccounting, $rootScope.failure)

  }

  function showLocation(employee) {

    if (employee.status === "active" && employee.employee.location_lat === '0' && employee.employee.location_lng === '0') {

      $ionicPopup.alert({title: "העובד לא אישר מיקום", buttons: [{text: 'אישור'}]});
      return;

    }

    if (employee.status === "active" && employee.employee.location_lat !== '0' && employee.employee.location_lng !== '0') {

      $state.go('app.map', {
        employee_id: employee.employee.index,
        lat: employee.employee.location_lat,
        lng: employee.employee.location_lng
      })

    }
  }


  function successSendAccounting(data) {

    if (data.status == 1)
      $ionicPopup.alert({title: 'הפעולה בוצעה בהצלחה', buttons: [{text: 'אישור'}]});

  }

  function updateAccounting() {

    $http.post(WebService.UPDATEACCOUNTING, {

      income_id: $rootScope.local_income.index,
      income_amount: $rootScope.accounting.income,
      expenses_id: $rootScope.local_expenses.index,
      expenses_amount: $rootScope.accounting.expenses

    }).then(successUpdateAccounting, $rootScope.failure)

  }

  function successUpdateAccounting(data) {

    if (data.status == 1)
      $ionicPopup.alert({title: 'הפעולה בוצעה בהצלחה', buttons: [{text: 'אישור'}]});

  }
}


function AttendanceCtrl($ionicLoading, $cordovaGeolocation, $cordovaDatePicker, $localStorage, $ionicPopup, AuthService, $rootScope, WebService, $http, $interval, $scope) {

  $scope.$on('$ionicView.enter', function () {

    $rootScope.connectionExists = typeof $localStorage.company_id !== 'undefined';

  });


  // Data

  $scope.clock = moment();
  $scope.newTime = {};
  $scope.employee_id = AuthService.getUser();
  $scope.liveCoordinates = {lat: '', lng: ''};

  // Clock

  $interval(function () {
    $scope.clock = moment();
  }, 1000);
  $interval(function () {
    if ($localStorage.mode === 1) {
      $ionicPopup.alert({
        title: 'האם אתה עדיין בזמן עבודה? אל תשכח לסגור את שעון הפעילות שלך בסוף יום העבודה.',
        buttons: [{text: 'אישור'}]
      });
    }
  }, 1000 * 60 * 60 * 4);

  // Methods

  $scope.showDecisionPopup = showDecisionPopup;
  $scope.hideDecisionPopup = hideDecisionPopup;
  $scope.isGPSenabled = isGPSenabled;
  $scope.subtractMonth = subtractMonth;
  $scope.addMonth = addMonth;
  $scope.updateTime = updateTime;
  $scope.showNoLocationPopup = showNoLocationPopup;

  // Functions

  function showNoLocationPopup() {

    $ionicPopup.alert({title: "העובד לא אישר מיקום", buttons: [{text: 'אישור'}]});

  }

  function subtractMonth() {
    $scope.today.subtract(1, 'month');
    $rootScope.getAttendance(AuthService.getUser());
  }

  function addMonth() {
    $scope.today.add(1, 'month');
    $rootScope.getAttendance(AuthService.getUser());
  }

  function showDecisionPopup(x) {

    $rootScope.mode = x;

    $scope.decisionPopup = $ionicPopup.show({
      templateUrl: 'templates/popups/decision.html',
      scope: $scope,
      cssClass: 'decisionPopup'
    });

  }

  function hideDecisionPopup() {

    $rootScope.mode = typeof $localStorage.mode !== 'undefined' ? $localStorage.mode : 0;

    $scope.decisionPopup.close();

  }

  function updateTime(index, time, type) {

    $scope.newTime[index] = time;

    var options = {
      date: moment($scope.newTime[index], 'HH:mm').toDate(),
      mode: 'time'
    };

    $cordovaDatePicker.show(options).then(function (data) {

      $http.post(WebService.UPDATEHOURS, {

        index: index,
        time: moment(data).format('HH:mm'),
        // date: moment(data).format('YYYY-MM-DD'),
        type: type

      }).then(successUpdateTime, $rootScope.failure)

    });

  }

  function successUpdateTime(data) {

    if (data.status == '1') {

      $rootScope.getAttendance(AuthService.getUser());
      $scope.newTime = {};

    }
    else
      $rootScope.failure();

  }

  function isGPSenabled() {

    if (window.cordova) {

      $ionicLoading.show();

      CheckGPS.check(function win() {

          var posOptions = {timeout: 3000, enableHighAccuracy: true};

          $cordovaGeolocation.getCurrentPosition(posOptions)
            .then(function (position) {

              sendTime(position.coords.latitude, position.coords.longitude);

            }, function (err) {
              sendTime(0, 0);
            });

        },

        function fail() {       // if there is no GPS

          $ionicLoading.hide();

          $ionicPopup.confirm({            // show alert
            title: 'Your GPS is disabled.',
            template: '<div style="text-align: center;">Are you sure that you want to continue with GPS disabled?</div>'
          }).then(function (res) {
            if (res) {
              sendTime(0, 0);
            }
          })

        })

    } else {

      sendTime(0, 0);

    }

  }

  function sendTime(lat, lng) {

    $scope.liveCoordinates.lat = lat;
    $scope.liveCoordinates.lng = lng;

    $http.post(WebService.SENDTIME, {

      user: AuthService.getUser(),
      time: moment().format('HH:mm'),
      type: $rootScope.mode,
      date: moment().format('YYYY-MM-DD'),
      lat: lat,
      lng: lng

    }).then(successSendTime, failureSendTime)

  }

  function successSendTime(data) {

    $scope.decisionPopup.close();

    if (data.status == '1') {

      $rootScope.getAttendance(AuthService.getUser());
      $rootScope.updateLiveLocation($scope.liveCoordinates.lat, $scope.liveCoordinates.lng);
      $localStorage.mode = $rootScope.mode;

    } else {

      $ionicPopup.alert({title: "נתונים לא נשמרו!", buttons: [{text: 'אישור'}]});
      $rootScope.mode = $localStorage.mode;

    }

  }

  function failureSendTime() {

    $scope.decisionPopup.close();

    $ionicPopup.alert({title: "אין התחברות!", buttons: [{text: 'אישור'}]});

    $rootScope.mode = $localStorage.mode;

  }

}


function BalanceCtrl($rootScope, $scope) {

  $scope.doRefresh = function () {

    $rootScope.getBalance()
      .then($rootScope.getExpenses()
        .then($rootScope.getIncome()))
      .finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
      });

  };

}


function ConnectionCtrl($state, $ionicPopup, $rootScope, AuthService, $http, WebService, $localStorage, $scope, CheckService) {

  $scope.$on('$ionicView.enter', function (e) {

    $rootScope.connectionExists = typeof $localStorage.company_id !== 'undefined' ? true : false;

    if ($rootScope.connectionExists)
      $scope.company_id = $localStorage.company_id;

  });

  // Data

  $scope.connection = {number: ''};

  // Methods

  $scope.setConnection = setConnection;
  $scope.deleteConnection = deleteConnection;

  // Functions

  function setConnection() {

    if (!CheckService.isAnyFieldEmpty($scope.connection)) {

      $http.post(WebService.UPDATECONNECTION, {

        employee_id: AuthService.getUser(),
        company_id: $scope.connection.number,
        event: 'connected'

      }).then(successSetConnection, $rootScope.failure)

    }

  }

  function successSetConnection(data) {

    if (data.status == '1') {

      $ionicPopup.alert({title: "החיבור עבר בהצלחה, מחכים לאישור העסק.", buttons: [{text: 'אישור'}]});

      $scope.connection = {number: ''};

    } else

      $rootScope.failure();

  }

  function deleteConnection() {

    $http.post(WebService.UPDATECONNECTION, {

      employee_id: AuthService.getUser(),
      company_id: $localStorage.company_id,
      event: 'disconnected'

    }).then(successDeleteConnection, $rootScope.failure)

  }

  function successDeleteConnection(data) {

    if (data.status == '1') {

      $ionicPopup.alert({title: 'הפעולה בוצעה בהצלחה', buttons: [{text: 'אישור'}]});

      delete $localStorage.company_id;
      $scope.connectionExists = false;

    } else
      $rootScope.failure();

  }

}


function EmployeesCtrl($state, $localStorage, $ionicPopup, $ionicPopover, AuthService, $scope, $http, WebService, $rootScope, $interval) {

  $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

    $scope.popover.hide();

  });

  $scope.employee = null;

  $interval(function () {
    $rootScope.getActiveEmployees(AuthService.getUser(), true)
  }, 1000 * 15);

  $scope.doRefresh = function () {

    $rootScope.getActiveEmployees(AuthService.getUser(), false).finally(function () {
      $scope.$broadcast('scroll.refreshComplete');
    });

  };

  $scope.showEmployeePopover = showEmployeePopover;
  $scope.deleteEmployee = deleteEmployee;
  $scope.sendExcel = sendExcel;
  $scope.showNoLocationPopup = showNoLocationPopup;
  $scope.getLiveLocation = getLiveLocation;

  $ionicPopover.fromTemplateUrl('templates/popups/employee_popover.html', {scope: $scope})
    .then(function (popover) {
      $scope.popover = popover;
    });

  function getLiveLocation(index) {

    $http.post(WebService.LIVE, {user: index}).then(successGetLiveLocation, $rootScope.failure)

  }

  function successGetLiveLocation(data) {

    $state.go('app.map', {employee_id: data.index, lat: data.live_lat, lng: data.live_lng});

  }

  function showEmployeePopover($event, employee) {

    $scope.popover.show($event);
    $scope.employee = employee;

  }

  function deleteEmployee() {

    $http.post(WebService.UPDATECONNECTION, {

      employee_id: $scope.employee.employee.index,
      company_id: AuthService.getUser(),
      event: 'disconnected'

    }).then(successDeleteEmployee, $rootScope.failure)

  }

  function successDeleteEmployee() {

    $scope.popover.hide();
    $ionicPopup.alert({title: "הפעולה בוצעה בהצלחה", buttons: [{text: 'אישור'}]}).then(function () {
      $rootScope.getActiveEmployees(AuthService.getUser());
    })

  }

  function showNoLocationPopup() {

    $scope.popover.hide();
    $ionicPopup.alert({title: "העובד לא אישר מיקום", buttons: [{text: 'אישור'}]});

  }


  function sendExcel() {

    var proms = [];
    var employeesRows = {};

    // run on each employee and put the basic data on it
    angular.forEach($rootScope.employees, function (employee) {
      employeesRows[employee.employee.index] = {
        id: employee.employee.index,
        name: employee.employee.firstname + " " + employee.employee.lastname,
        salPerHour: employee.employeeData.salary.amount,
        bonus: employee.employeeData.salary.other_expenses
      };
    });

    // on each employee get his data of the sallary of the last month
    $rootScope.employeesRows = employeesRows;
    for (var row in employeesRows) {
      proms.push($rootScope.getAttendanceLastMonth(row).then(function (result) {
        return result;
      }));
    }

    // run on each employee after getting all data for excel. and put it at the format of the excel service as body request paramter
    Promise.all(proms).then(function () {
      var mail = AuthService.getMail();
      var total_money = 0;
      var rows = [];
      rows.push(["סיכום שעות חודשי לעובדים"]);
      rows.push([""]);
      rows.push(["שם העובד", "שכר לשעה", "תוספות", "סך שעות", "סכום כללי", "סכום כולל תוספות", "סך ימי עבודה"]);
      rows.push([""]);

      for (var i in $rootScope.employees) {
        var employee = proms[i].$$state.value[$rootScope.employees[i].employee.index];
        if(employee === undefined) continue;
        var row = [];
        row.push(employee.name);
        row.push(employee.salPerHour);
        row.push(employee.bonus);
        row.push(employee.sumHours);
        row.push(employee.sumSal);
        row.push(employee.sumMoney);
        row.push(employee.sumDays);
        rows.push(row);
        total_money += employee.sumMoney;
      }
      rows.push([""]);
      rows.push(["סיכום סכום משכורות"]);
      rows.push([total_money]);

      $http.post(WebService.SENDEXCEL, {
        mail: AuthService.getMail(),
        rows: rows
      }).then(successSendExcel, $rootScope.failure)
    });
  }


  function successSendExcel(data) {

    // if (data.status === '1')
      $ionicPopup.alert({title: "דוח האקסל נשלח למייל: " + $localStorage.email, buttons: [{text: 'אישור'}]});
    // else
    //   $ionicPopup.alert({title: "בעיה בהתחברות", buttons: [{text: 'אישור'}]});

  }

}


function HoursCtrl($cordovaDatePicker, $localStorage, $ionicPopup, CheckService, $stateParams, AuthService, $scope, $http, WebService, $rootScope) {

  $scope.$on('$ionicView.enter', function () {

    angular.forEach($rootScope.employees, function (employee) {

      if (employee.employee.index == $stateParams.employee_id) {
        console.log(employee);
        $scope.employeeAttendance = employee.attendance;
      }


    })

  });


  // Data

  $scope.employee_id = $stateParams.employee_id;
  $scope.newTime = {};

  // Methods

  $scope.sendTime = sendTime;
  $scope.subtractMonth = subtractMonth;
  $scope.addMonth = addMonth;
  $scope.updateTime = updateTime;
  $scope.sendExcel = sendExcel;

  $scope.doRefresh = function () {

    $rootScope.getAttendance($stateParams.employee_id).then(function (result) {
      $scope.employeeAttendance = result;
    })

      .finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
      });

  };

  // Functions

  function subtractMonth() {
    $rootScope.today.subtract(1, 'month');
    $rootScope.getAttendance($scope.employee_id).then(function (result) {
      $scope.employeeAttendance = result;
    });
  }

  function addMonth() {
    $rootScope.today.add(1, 'month');
    $rootScope.getAttendance($scope.employee_id).then(function (result) {
      $scope.employeeAttendance = result;
    });
  }

  function updateTime(index, time, type) {

    $scope.newTime[index] = time;

    var options = {
      date: moment($scope.newTime[index], 'HH:mm').toDate(),
      mode: 'time'
    };

    $cordovaDatePicker.show(options).then(function (data) {

      $http.post(WebService.UPDATEHOURS, {

        index: index,
        time: moment(data).format('HH:mm'),
        type: type

      }).then(successUpdateTime, $rootScope.failure)

    });

  }

  function successUpdateTime(data) {

    if (data.status == '1') {

      $rootScope.getAttendance($scope.employee_id).then(function (result) {
        $scope.employeeAttendance = result;
      });
      $scope.newTime = {};

    }
    else
      $rootScope.failure();

  }

  function sendTime() {

    $http.post(WebService.SENDTIME, {

      user: $scope.employee_id,
      time: moment().format('HH:mm'),
      type: $rootScope.mode,
      date: moment().format('YYYY-MM-DD')

    }).then(successSendTime, failureSendTime)

  }

  function successSendTime(data) {

    $scope.decisionPopup.close();

    if (data.status == '1') {

      $rootScope.getAttendance($scope.employee_id).then(function (result) {
        $scope.employeeAttendance = result;
      });
      $localStorage.mode = $rootScope.mode;

    } else {
      $ionicPopup.alert({title: "נתונים לא נשמרו!", buttons: [{text: 'אישור'}]});
    }

    $rootScope.mode = $localStorage.mode;

  }

  function failureSendTime() {

    $scope.decisionPopup.close();

    $ionicPopup.alert({title: "אין התחברות!", buttons: [{text: 'אישור'}]});

    $rootScope.mode = $localStorage.mode;

  }

  function sendExcel() {

    $http.post(WebService.EXCEL, {

      employee_ids: [$scope.employee_id],
      company_id: AuthService.getUser(),
      start: moment($rootScope.today).startOf('month').format('YYYY-MM-DD'),
      end: moment($rootScope.today).endOf('month').format('YYYY-MM-DD')

    }).then(successSendExcel, $rootScope.failure)

  }

  function successSendExcel(data) {

    if (data.status === '1')
      $ionicPopup.alert({title: "דוח האקסל נשלח למייל: " + $localStorage.email, buttons: [{text: 'אישור'}]});
    else
      $ionicPopup.alert({title: "בעיה בהתחברות", buttons: [{text: 'אישור'}]});

  }

}


function ExpensesCtrl($ionicPopup, WebService, $http, $ionicModal, $rootScope, $scope, AuthService, $state, $interval) {

  $scope.newExpenses = {description: '', amount: '', type: false};
  $scope.active = '';

  $scope.openExpensesModal = openExpensesModal;
  $scope.closeExpensesModal = closeExpensesModal;
  $scope.addExpenses = addExpenses;
  $scope.toggleGroup = toggleGroup;
  $scope.openDeletePopup = openDeletePopup;

  $interval(function () {
    $rootScope.getExpenses(true)
  }, 1000 * 15);

  $scope.doRefresh = function () {

    $rootScope.getExpenses(false).finally(function () {
      $scope.$broadcast('scroll.refreshComplete');
    });

  };

  function toggleGroup(x) {

    if ($scope.active == x)
      $scope.active = '';
    else
      $scope.active = x;

  }

  function openExpensesModal() {

    $ionicModal.fromTemplateUrl('templates/modals/expenses_modal.html', {scope: $scope, animation: 'slide-in-up'})

      .then(function (modal) {

        $scope.modal = modal;
        $scope.modal.show();

      });

  }

  function closeExpensesModal() {

    $scope.newExpenses = {description: '', amount: '', type: false};
    $rootScope.getExpenses();
    $rootScope.getBalance();
    $scope.modal.hide();

  }

  function addExpenses() {

    $http.post(WebService.ADDEXPENSES, {

      company_id: AuthService.getUser(),
      description: $scope.newExpenses.description || "ללא תיאור",
      amount: $scope.newExpenses.amount,
      type: $scope.newExpenses.type == true ? 1 : 0,
      from_employee: 0,
      employee_id: 0,
      date: moment().format('YYYY-MM-DD')


    }).then(successAddExpenses, $rootScope.failure)

  }

  function successAddExpenses(data) {

    if (data.status == 1) {

      $ionicPopup.alert({title: "הפעולה בוצעה בהצלחה", buttons: [{text: 'אישור'}]});
      $scope.newExpenses = {description: '', amount: '', type: false};

    }

  }

  function openDeletePopup(index) {

    $ionicPopup.confirm({
      template: '<div align="center">Are you sure?</div>',
      buttons: [
        {text: 'ביטול', type: 'button-assertive'},
        {
          text: 'אישור', type: 'button-default', onTap: function (res) {
          if (res) {
            deleteExpenses(index)
          }
        }
        }
      ]
    })

  }

  function deleteExpenses(index) {

    $http.post(WebService.DELETEEXPENSES, {expenses_id: index}).then(function () {
      $rootScope.getExpenses();
      $rootScope.getBalance();
    }, $rootScope.failure);

  }

}

function FlowCtrl($rootScope, AuthService, $scope, $http, WebService) {
}


function IncomeCtrl($ionicPopup, WebService, $http, $ionicModal, $rootScope, $scope, AuthService, $interval) {

  $scope.newIncome = {description: '', amount: '', type: false};
  $scope.active = '';

  $scope.openIncomeModal = openIncomeModal;
  $scope.closeIncomeModal = closeIncomeModal;
  $scope.addIncome = addIncome;
  $scope.toggleGroup = toggleGroup;
  $scope.openDeletePopup = openDeletePopup;

  $interval(function () {
    $rootScope.getIncome(true)
  }, 1000 * 15);

  $scope.doRefresh = function () {

    $rootScope.getIncome(false).finally(function () {
      $scope.$broadcast('scroll.refreshComplete');
    });

  };

  function toggleGroup(x) {

    if ($scope.active == x)
      $scope.active = '';
    else
      $scope.active = x;

  }

  function openIncomeModal() {

    $ionicModal.fromTemplateUrl('templates/modals/income_modal.html', {scope: $scope, animation: 'slide-in-up'})

      .then(function (modal) {

        $scope.modal = modal;
        $scope.modal.show();

      });

  }

  function closeIncomeModal() {

    $scope.newIncome = {description: '', amount: '', type: false};
    $rootScope.getIncome();
    $rootScope.getBalance();
    $scope.modal.hide();

  }

  function addIncome() {

    $http.post(WebService.ADDINCOME, {

      company_id: AuthService.getUser(),
      description: $scope.newIncome.description,
      amount: $scope.newIncome.amount,
      from_employee: 0,
      employee_id: 0,
      date: moment().format('YYYY-MM-DD')


    }).then(successAddIncome, $rootScope.failure)

  }

  function successAddIncome(data) {

    if (data.status == 1) {

      $ionicPopup.alert({title: "הפעולה בוצעה בהצלחה", buttons: [{text: 'אישור'}]});
      $scope.newIncome = {description: '', amount: '', type: false};

    }

  }

  function openDeletePopup(index) {

    $ionicPopup.confirm({template: '<div align="center">אתה בטוח?</div>'})

      .then(function (res) {

        if (res) {

          $http.post(WebService.DELETEINCOME, {income_id: index}).then(function () {
            $rootScope.getIncome();
            $rootScope.getBalance();
          }, $rootScope.failure);

        }
      });

  }

}


function LocationCtrl($rootScope, $scope, $stateParams) {

  $scope.$on('$ionicView.enter', function () {

    angular.forEach($rootScope.employees, function (employee) {

      if (employee.employee.index == $stateParams.employee_id) {
        if (typeof employee.location === 'undefined' || employee.location.lat === '' || employee.location.lat === 0 || employee.location.lat === '0') {
          alert("העובד לא אישר מיקום");
          location.href = '/#/app/employees';
        }
        else {
          $scope.coordinates = {lat: Number(employee.location.lat), lng: Number(employee.location.lng)};
          initMap();
        }


      }

    })

  });

  // Data

  $scope.coordinates = {lat: 0, lng: 0};

  $scope.doRefresh = function () {

    $rootScope.getEmployee($stateParams.employee_id).then(function (result) {
      employee.location = {lat: result.data.lat, lng: result.data.lng};
    })
      .finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
      });

  };

  function initMap() {

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 12,
      center: $scope.coordinates
    });

    var marker = new google.maps.Marker({
      position: $scope.coordinates,
      map: map
    });
  }

}


function LoginCtrl($ionicLoading, $q, $http, WebService, $ionicModal, $localStorage, $scope, $rootScope, AuthService, CheckService, $ionicPopup, $state) {

  // Data

  $scope.user = {email: '', password: ''};
  $scope.forgot = {email: ''};

  // Methods

  $scope.makeLogin = makeLogin;
  $scope.openForgotPasswordModal = openForgotPasswordModal;
  $scope.closeForgotPasswordModal = closeForgotPasswordModal;
  $scope.sendPassword = sendPassword;

  // Functions

  function makeLogin() {

    if (!CheckService.isAnyFieldEmpty($scope.user)) {

      AuthService.authenticate($scope.user.email, $scope.user.password, $rootScope.push_id).then(successLogin, failureLogin);

    }

  }

  function successLogin() {

    switch ($localStorage.role) {

      case '0':
        $http.post(WebService.ALL, {company_id: AuthService.getUser()})

          .then(function (data) {

            console.log(data);

            $rootScope.balance = data[0];
            $rootScope.statistics = data[1];
            $rootScope.company = {

              firstname: data[2].firstname,
              lastname: data[2].lastname,
              phone: Number(data[2].phone),
              birthday: moment(data[2].birthday, 'DD/MM/YYYY').toDate(),
              password: '',
              title: data[2].title,
              address: data[2].address,
              employee_count: data[2].employee_count,
              desc: data[2].desc

            };
            $rootScope.employees = data[3];
            $rootScope.activeEmployees = data[4];
            $rootScope.permissions = data[5];
            $rootScope.expenses = data[6];
            $rootScope.income = data[7];

          })

          .then(function () {

            angular.forEach($rootScope.employees, function (employee) {

              $rootScope.getAttendance(employee.employee.index).then(function (result) {

                employee.attendance = result;

                $rootScope.getEmployee(employee.employee.index).then(function (result) {

                  employee.location = {lat: result.data.lat, lng: result.data.lng};
                  employee.profile = {
                    firstname: result.data.firstname,
                    lastname: result.data.lastname,
                    birthday: result.data.birthday,
                    phone: result.data.phone,
                    salary: result.salary.amount,
                    other_expenses: result.salary.other_expenses
                  };

                  $rootScope.getEmployeeData(employee.employee.index).then(function (result) {
                    employee.employeeData = result;
                  });

                });

              });

            });

            $ionicPopup.alert({title: "מספר העוסק שלך: " + $localStorage.company_id, buttons: [{text: 'אישור'}]})

              .then(function () {
                $state.go('app.balance');
              })

          }, function () {
            $ionicLoading.hide();
            $ionicPopup.alert({title: "אין התחברות!", buttons: [{text: 'אישור'}]});
          });

        break;

      case '1':

        $http.post(WebService.EMPLOYEEALL, {

          user: AuthService.getUser(),
          start: moment($rootScope.today).startOf('month').format('YYYY-MM-DD'),
          end: moment($rootScope.today).endOf('month').format('YYYY-MM-DD'),
          date: moment().isAfter(moment('05:59', 'HH:mm'), 'hours') ? moment().format('YYYY-MM-DD') : moment().subtract('1', 'day').format('YYYY-MM-DD'),
          company_id: $rootScope.company_id

        }).then(function (data) {

          console.log(data);

          for (var i = 0; i < data[0].length; i++) {

            if (data[0][i].end != '') {

              var checkin = moment(data[0][i].start_date + ' ' + data[0][i].start, 'YYYY-MM-DD HH:mm');
              var checkout = moment(data[0][i].end_date + ' ' + data[0][i].end, 'YYYY-MM-DD HH:mm');

              // if (checkout < checkin)
              //   checkout.add(1, 'day');

              var duration = moment.duration(checkout.diff(checkin));

              data[0][i].total = CheckService.formatDuration(duration);
              $localStorage.mode = 0;
              $rootScope.mode = $localStorage.mode;

            } else {

              data[0][i].end = '-';
              data[0][i].total = '-';
              $localStorage.mode = 1;
              $rootScope.mode = $localStorage.mode;

            }

            data[0][i].date = moment(data[0][i].date, 'YYYY-MM-DD').format('DD/MM/YYYY')

          }

          $rootScope.attendance = data[0];

          $rootScope.employee = {
            data: {
              firstname: data[1].firstname,
              lastname: data[1].lastname,
              password: '',
              phone: Number(data[1].phone),
              birthday: moment(data[1].birthday, 'DD/MM/YYYY').toDate(),
              lat: data[1].location_lat,
              lng: data[1].location_lng
            },
            salary: {
              amount: parseFloat(data[1].salary.amount),
              other_expenses: parseFloat(data[1].salary.other_expenses)
            },
            point: {
              gender: String(data[1].point.gender),
              family_status: data[1].point.family_status,
              young_volunteer: data[1].point.young_volunteer,
              children_quantity: data[1].point.children_quantity,
              children_possession: data[1].point.children_possession,
              alimony: data[1].point.alimony,
              children_invalids: data[1].point.children_invalids,
              ole_hadash: data[1].point.ole_hadash,
              soldier: data[1].point.soldier,
              national_service: data[1].point.national_service,
              bachelor: data[1].point.bachelor,
              master: data[1].point.master,
              doctor: data[1].point.doctor
            }
          };

          $rootScope.flow = data[2];

          for (var k = 0; k < data[3].attendance.length; k++) {

            data[3].attendance[k].date = moment(data[3].attendance[k].date, 'YYYY-MM-DD').format('DD/MM/YYYY');

            if (data[3].attendance[k].end != '') {

              var checkin = moment(data[3].attendance[k].start_date + ' ' + data[3].attendance[k].start, 'YYYY-MM-DD HH:mm');
              var checkout = moment(data[3].attendance[k].end_date + ' ' + data[3].attendance[k].end, 'YYYY-MM-DD HH:mm');

              // if (checkout < checkin)
              //   checkout.add(1, 'day');

              var duration = moment.duration(checkout.diff(checkin));
              var arr = CheckService.formatDuration(duration).split('.');

              data[3].attendance[k].total = parseFloat(arr[0] * data[3].salary.amount + ((arr[1] / 6) * 10) * (data[3].salary.amount / 100));

            } else {

              data[3].attendance[k].end = '-';
              data[3].attendance[k].total = '-'

            }

          }

          $rootScope.employeeData = data[3];

          if (data[4][0] != null && data[4][1] != null) {

            // change here
            $rootScope.local_income = data[4][0];
            // change here
            $rootScope.local_expenses = data[4][1];
            // change here
            $rootScope.accounting.income = Number($rootScope.local_income.amount);
            // change here
            $rootScope.accounting.expenses = Number($rootScope.local_expenses.amount);

          }

          $rootScope.activeEmployees = data[5];

        });

        $state.go('app.attendance');
        break;

    }

    $scope.user = {email: '', password: ''};

  }


  function failureLogin(errors) {

    if (!errors) {

      $rootScope.failure();
      return;

    }

    if (errors.error == 'invalidCredentials')

      $ionicPopup.alert({title: "נתונים לא נכונים!", buttons: [{text: 'אישור'}]});


    // if (errors.error == 'notApproved')

    // $ionicPopup.alert({title: "The account is still not approved!"});

  }

  function openForgotPasswordModal() {

    $ionicModal.fromTemplateUrl('templates/modals/forgot_password_modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    })
      .then(function (modal) {

        $scope.modal = modal;
        $scope.modal.show();

      });

  }

  function closeForgotPasswordModal() {
    $scope.modal.hide();
  }

  function sendPassword() {

    if ($scope.forgot.email === '') {

      $ionicPopup.alert({title: "נא למלא את כל השדות", buttons: [{text: 'אישור'}]});
      return;

    }

    $http.post(WebService.FORGOT, {email: $scope.forgot.email}).then(successSendPassword, $rootScope.failure);

  }

  function successSendPassword(data) {

    if (data.status === '1') {

      $ionicPopup.alert({title: "בדוק את האימייל שלך", buttons: [{text: 'אישור'}]}).then(function () {

        $scope.forgot = {email: ''};
        $scope.modal.hide();

      });

    } else
      $ionicPopup.alert({title: "פרטים שגויים, אנא נסה שנית", buttons: [{text: 'אישור'}]});

  }

}

function MapCtrl($ionicPopup, $rootScope, $scope, $stateParams, $state) {

  $scope.$on('$ionicView.enter', function () {

    $scope.coordinates = {lat: Number($stateParams.lat), lng: Number($stateParams.lng)};
    initMap();

  });

  // Data

  $scope.coordinates = {lat: 0, lng: 0};

  function initMap() {

    var map = new google.maps.Map(document.getElementById('map2'), {
      zoom: 12,
      center: $scope.coordinates
    });

    var marker = new google.maps.Marker({
      position: $scope.coordinates,
      map: map
    });
  }

}

function PermissionsCtrl($ionicPopup, $localStorage, WebService, $rootScope, AuthService, $http, $scope) {

  $scope.doRefresh = function () {

    $rootScope.getApprovedEmployees().finally(function () {
      $scope.$broadcast('scroll.refreshComplete');
    });

  };

  $scope.updatePermission = updatePermission;

  function updatePermission(employee_id, x) {

    $http.post(WebService.UPDATEPERMISSION, {
      employee_id: employee_id,
      permission: x
    }).then(successUpdatePermission, $rootScope.failure);

  }

  function successUpdatePermission(data) {

    if (data.status == '1')
      $rootScope.getApprovedEmployees();
    else
      $rootScope.failure();

  }

}


function ProfileCompanyCtrl($ionicPopup, $rootScope, $scope, AuthService, $http, WebService) {

  // Methods

  $scope.updateCompany = updateCompany;

  // Functions

  function updateCompany() {

    $http.post(WebService.UPDATECOMPANY, {

      user: AuthService.getUser(),
      firstname: $rootScope.company.firstname,
      lastname: $rootScope.company.lastname,
      phone: $rootScope.company.phone,
      birthday: moment($rootScope.company.birthday).format('DD/MM/YYYY'),
      password: $rootScope.company.password,
      title: $rootScope.company.title,
      address: $rootScope.company.address.formatted_address ? $rootScope.company.address.formatted_address : $rootScope.company.address,
      employee_count: $rootScope.company.employee_count,
      desc: $rootScope.company.desc

    }).then(successUpdateCompany, $rootScope.failure)

  }

  function successUpdateCompany(data) {

    if (data.status == '1') {

      $ionicPopup.alert({title: "הפעולה בוצעה בהצלחה", buttons: [{text: 'אישור'}]});
      $scope.company.password = '';

    } else
      $rootScope.failure();

  }

}


function ProfileEmployeeCtrl($ionicPopup, $rootScope, $scope, AuthService, $http, WebService) {

  // Methods

  $scope.updateEmployee = updateEmployee;

  // Functions

  function updateEmployee() {

    $rootScope.employee.user = AuthService.getUser();
    $rootScope.employee.data.birthday = moment($rootScope.employee.data.birthday).format('DD/MM/YYYY');

    $http.post(WebService.UPDATEEMPLOYEE, $rootScope.employee).then(successUpdateEmployee, $rootScope.failure)

  }

  function successUpdateEmployee(data) {

    console.log(data);
    return;

    if (data.status == '1') {

      $ionicPopup.alert({title: "הפעולה בוצעה בהצלחה", buttons: [{text: 'אישור'}]});
      $rootScope.employee.password = '';
      $rootScope.getEmployee();

    } else
      $rootScope.failure();

  }

}

function ProfileForManagerCtrl($stateParams, $rootScope, $scope) {

  $scope.$on('$ionicView.enter', function () {

    angular.forEach($rootScope.employees, function (employee) {

      if (employee.employee.index == $stateParams.employee_id)
        $scope.pfmEmployee = employee.profile;

    })

  });

  $scope.doRefresh = function () {

    $rootScope.getEmployee($stateParams.employee_id).then(function (result) {

      $scope.pfmEmployee = {
        firstname: result.data.firstname,
        lastname: result.data.lastname,
        birthday: result.data.birthday,
        phone: result.data.phone,
        salary: result.salary.amount,
        other_expenses: result.salary.other_expenses
      };

    })

      .finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
      });

  };
}


function RegisterCtrl(CheckService, $localStorage, $state, $rootScope, WebService, $http, $scope, $stateParams, $ionicSideMenuDelegate, $ionicPopup) {

  $ionicSideMenuDelegate.canDragContent(false);

  // Data

  $localStorage.role = $stateParams['role'];
  $rootScope.role = $localStorage.role;
  $scope.title = $rootScope.role === '0' ? 'הרשמת בית עסק' : 'הרשמת עובד';
  $scope.newUser = {firstname: '', lastname: '', email: '', password: '', phone: '', birthday: ''};
  $scope.gender = {chosen: ''};
  $scope.money = {salary: '', others: ''};

  // Methods

  $scope.chooseGender = chooseGender;
  $scope.makeRegistration = makeRegistration;

  // Functions

  function chooseGender(x) {
    $scope.gender.chosen = x;
  }

  function makeRegistration() {

    $scope.newUser.birthday = moment($scope.newUser.birthday).format('DD/MM/YYYY');
    $scope.newUser.type = $localStorage.role;
    $scope.newUser.gender = $scope.gender.chosen === '' ? null : $scope.gender.chosen;

    console.log($scope.newUser);

    if ($localStorage.role == '1') {

      if (!CheckService.isAnyFieldEmpty($scope.money)) {

        $scope.newUser.salary = $scope.money.salary;
        $scope.newUser.others = $scope.money.others;

        sendData();

      }

    } else
      sendData();

  }

  function sendData() {

    if (!CheckService.isAnyFieldEmpty($scope.newUser))

      $scope.newUser.push_id = $rootScope.push_id;
    $http.post(WebService.REGISTER, $scope.newUser).then(successRegistration, $rootScope.failure)

  }

  function successRegistration(data) {

    console.log(data);

    if (data.status === '1') {

      if ($localStorage.role == '0') {
        $localStorage.company_id = data.id;
        $rootScope.company_id = $localStorage.company_id;
      }
      else
        $localStorage.employee_id = data.id;

      $localStorage.firstname = $scope.newUser.firstname;
      $localStorage.lastname = $scope.newUser.lastname;
      $localStorage.email = $scope.newUser.email;
      $localStorage.phone = $scope.newUser.phone;
      $localStorage.gender = $scope.newUser.gender;
      $localStorage.birthday = $scope.newUser.birthday;

      $scope.newUser = {firstname: '', lastname: '', email: '', password: '', phone: '', birthday: ''};
      $scope.gender = {chosen: ''};

      if ($rootScope.role == '0')
        $state.go('app.register_additional', {role: $rootScope.role});
      else
        $state.go('app.register_points1', {role: $rootScope.role})


    } else if (data.status === '0') {

      $ionicPopup.alert({
        title: "אתה כבר משתמש רשום, אנא לחץ על ׳שכחתי סיסמא׳ בכדי לשחזר את פרטייך",
        buttons: [{text: 'אישור'}]
      }).then(function () {

        $scope.newUser = {firstname: '', lastname: '', email: '', password: '', phone: '', birthday: ''};
        $scope.gender = {chosen: ''};
        $state.go('app.login')

      });

    } else

      $rootScope.failure();

  }

}

function RegisterAdditionalCtrl($ionicLoading, $q, $state, $ionicPopup, AuthService, $http, WebService, CheckService, $ionicSideMenuDelegate, $localStorage, $scope, $stateParams, $rootScope) {

  $ionicSideMenuDelegate.canDragContent(false);

  // Data

  $localStorage.role = $stateParams['role'];
  $rootScope.role = $localStorage.role;
  $scope.autocompleteOptions = {componentRestrictions: {country: 'il'}};

  $scope.company = {title: '', address: '', quantity: '', area: ''};
  $scope.connection = {number: ''};

  // Methods

  $scope.makeEnterpriseRegistration = makeEnterpriseRegistration;
  $scope.setConnection = setConnection;
  $scope.skip = skip;

  // Functions

  function makeEnterpriseRegistration() {

    if (!CheckService.isAnyFieldEmpty($scope.company)) {

      $http.post(WebService.REGISTERENTERPRISE, {

        id: AuthService.getUser(),
        title: $scope.company.title,
        address: $scope.company.address.formatted_address,
        quantity: $scope.company.quantity,
        area: $scope.company.area

      }).then(successEnterpriseRegistration, $rootScope.failure)

    }

  }

  function successEnterpriseRegistration(data) {

    if (data.status == '1') {

      $ionicPopup.alert({title: "ההרשמה בוצעה בהצלחה", buttons: [{text: 'אישור'}]}).then(function () {

        $http.post(WebService.ALL, {company_id: AuthService.getUser()})

          .then(function (data) {

            console.log(data);

            $rootScope.balance = data[0];
            $rootScope.statistics = data[1];
            $rootScope.company = {

              firstname: data[2].firstname,
              lastname: data[2].lastname,
              phone: Number(data[2].phone),
              birthday: moment(data[2].birthday, 'DD/MM/YYYY').toDate(),
              password: '',
              title: data[2].title,
              address: data[2].address,
              employee_count: data[2].employee_count,
              desc: data[2].desc

            };
            $rootScope.employees = data[3];
            $rootScope.activeEmployees = data[4];
            $rootScope.permissions = data[5];
            $rootScope.expenses = data[6];
            $rootScope.income = data[7];

            $ionicPopup.alert({title: "מספר העוסק שלך: " + $localStorage.company_id, buttons: [{text: 'אישור'}]})

              .then(function () {
                $state.go('app.balance');
              })

          })

      });

    } else

      $ionicPopup.alert({title: "ההתחברות נכשלה", buttons: [{text: 'אישור'}]});

  }

  function setConnection() {

    if (!CheckService.isAnyFieldEmpty($scope.connection)) {

      $http.post(WebService.UPDATECONNECTION, {

        employee_id: AuthService.getUser(),
        company_id: $scope.connection.number,
        event: 'connected'

      }).then(successSetConnection, $rootScope.failure)

    }

  }

  function successSetConnection(data) {

    if (data.status == '1')
      $ionicPopup.alert({title: "התחברות לבית העסק בוצעה בהצלחה", buttons: [{text: 'אישור'}]}).then(function () {

        $http.post(WebService.EMPLOYEEALL, {

          user: AuthService.getUser(),
          start: moment($rootScope.today).startOf('month').format('YYYY-MM-DD'),
          end: moment($rootScope.today).endOf('month').format('YYYY-MM-DD'),
          date: moment().isAfter(moment('05:59', 'HH:mm'), 'hours') ? moment().format('YYYY-MM-DD') : moment().subtract('1', 'day').format('YYYY-MM-DD'),
          company_id: $rootScope.company_id

        }).then(function (data) {

          console.log(data);

          for (var i = 0; i < data[0].length; i++) {

            if (data[0][i].end != '') {

              var checkin = moment(data[0][i].start_date + ' ' + data[0][i].start, 'YYYY-MM-DD HH:mm');
              var checkout = moment(data[0][i].end_date + ' ' + data[0][i].end, 'YYYY-MM-DD HH:mm');

              // if (checkout < checkin)
              //   checkout.add(1, 'day');

              var duration = moment.duration(checkout.diff(checkin));

              data[0][i].total = CheckService.formatDuration(duration);
              $localStorage.mode = 0;
              $rootScope.mode = $localStorage.mode;

            } else {

              data[0][i].end = '-';
              data[0][i].total = '-';
              $localStorage.mode = 1;
              $rootScope.mode = $localStorage.mode;

            }

            data[0][i].date = moment(data[0][i].date, 'YYYY-MM-DD').format('DD/MM/YYYY')

          }

          $rootScope.attendance = data[0];

          $rootScope.employee = {
            data: {
              firstname: data[1].firstname,
              lastname: data[1].lastname,
              password: '',
              phone: Number(data[1].phone),
              birthday: moment(data[1].birthday, 'DD/MM/YYYY').toDate(),
              lat: data[1].location_lat,
              lng: data[1].location_lng
            },
            salary: {
              amount: parseFloat(data[1].salary.amount),
              other_expenses: parseFloat(data[1].salary.other_expenses)
            },
            point: {
              gender: String(data[1].point.gender),
              family_status: data[1].point.family_status,
              young_volunteer: data[1].point.young_volunteer,
              children_quantity: data[1].point.children_quantity,
              children_possession: data[1].point.children_possession,
              alimony: data[1].point.alimony,
              children_invalids: data[1].point.children_invalids,
              ole_hadash: data[1].point.ole_hadash,
              soldier: data[1].point.soldier,
              national_service: data[1].point.national_service,
              bachelor: data[1].point.bachelor,
              master: data[1].point.master,
              doctor: data[1].point.doctor
            }
          };

          $rootScope.flow = data[2];

          for (var k = 0; k < data[3].attendance.length; k++) {

            data[3].attendance[k].date = moment(data[3].attendance[k].date, 'YYYY-MM-DD').format('DD/MM/YYYY');

            if (data[3].attendance[k].end != '') {

              var checkin = moment(data[3].attendance[k].start_date + ' ' + data[3].attendance[k].start, 'YYYY-MM-DD HH:mm');
              var checkout = moment(data[3].attendance[k].end_date + ' ' + data[3].attendance[k].end, 'YYYY-MM-DD HH:mm');

              // if (checkout < checkin)
              //   checkout.add(1, 'day');

              var duration = moment.duration(checkout.diff(checkin));
              var arr = CheckService.formatDuration(duration).split('.');

              data[3].attendance[k].total = parseFloat(arr[0] * data[3].salary.amount + ((arr[1] / 6) * 10) * (data[3].salary.amount / 100));

            } else {

              data[3].attendance[k].end = '-';
              data[3].attendance[k].total = '-'

            }

          }

          $rootScope.employeeData = data[3];

          if (data[4][0] != null && data[4][1] != null) {

            // change here
            $rootScope.local_income = data[4][0];
            // change here
            $rootScope.local_expenses = data[4][1];
            // change here
            $rootScope.accounting.income = Number($rootScope.local_income.amount);
            // change here
            $rootScope.accounting.expenses = Number($rootScope.local_expenses.amount);

          }

          $rootScope.activeEmployees = data[5];

        });

        $state.go('app.attendance');

      });
    else
      $rootScope.failure();

  }

  function skip() {

    $http.post(WebService.EMPLOYEEALL, {

      user: AuthService.getUser(),
      start: moment($rootScope.today).startOf('month').format('YYYY-MM-DD'),
      end: moment($rootScope.today).endOf('month').format('YYYY-MM-DD'),
      date: moment().isAfter(moment('05:59', 'HH:mm'), 'hours') ? moment().format('YYYY-MM-DD') : moment().subtract('1', 'day').format('YYYY-MM-DD'),
      company_id: $rootScope.company_id

    }).then(function (data) {

      console.log(data);

      for (var i = 0; i < data[0].length; i++) {

        if (data[0][i].end != '') {

          var checkin = moment(data[0][i].start_date + ' ' + data[0][i].start, 'YYYY-MM-DD HH:mm');
          var checkout = moment(data[0][i].end_date + ' ' + data[0][i].end, 'YYYY-MM-DD HH:mm');

          /*if (checkout < checkin)
            checkout.add(1, 'day');*/

          var duration = moment.duration(checkout.diff(checkin));

          data[0][i].total = CheckService.formatDuration(duration);
          $localStorage.mode = 0;
          $rootScope.mode = $localStorage.mode;

        } else {

          data[0][i].end = '-';
          data[0][i].total = '-';
          $localStorage.mode = 1;
          $rootScope.mode = $localStorage.mode;

        }

        data[0][i].date = moment(data[0][i].date, 'YYYY-MM-DD').format('DD/MM/YYYY')

      }

      $rootScope.attendance = data[0];

      $rootScope.employee = {
        data: {
          firstname: data[1].firstname,
          lastname: data[1].lastname,
          password: '',
          phone: Number(data[1].phone),
          birthday: moment(data[1].birthday, 'DD/MM/YYYY').toDate(),
          lat: data[1].location_lat,
          lng: data[1].location_lng
        },
        salary: {
          amount: parseFloat(data[1].salary.amount),
          other_expenses: parseFloat(data[1].salary.other_expenses)
        },
        point: {
          gender: String(data[1].point.gender),
          family_status: data[1].point.family_status,
          young_volunteer: data[1].point.young_volunteer,
          children_quantity: data[1].point.children_quantity,
          children_possession: data[1].point.children_possession,
          alimony: data[1].point.alimony,
          children_invalids: data[1].point.children_invalids,
          ole_hadash: data[1].point.ole_hadash,
          soldier: data[1].point.soldier,
          national_service: data[1].point.national_service,
          bachelor: data[1].point.bachelor,
          master: data[1].point.master,
          doctor: data[1].point.doctor
        }
      };

      $rootScope.flow = data[2];

      for (var k = 0; k < data[3].attendance.length; k++) {

        data[3].attendance[k].date = moment(data[3].attendance[k].date, 'YYYY-MM-DD').format('DD/MM/YYYY');

        if (data[3].attendance[k].end != '') {

          var checkin = moment(data[3].attendance[k].start_date + ' ' + data[3].attendance[k].start, 'YYYY-MM-DD HH:mm');
          var checkout = moment(data[3].attendance[k].end_date + ' ' + data[3].attendance[k].end, 'YYYY-MM-DD HH:mm');

          // if (checkout < checkin)
          //   checkout.add(1, 'day');

          var duration = moment.duration(checkout.diff(checkin));
          var arr = CheckService.formatDuration(duration).split('.');

          data[3].attendance[k].total = parseFloat(arr[0] * data[3].salary.amount + ((arr[1] / 6) * 10) * (data[3].salary.amount / 100));

        } else {

          data[3].attendance[k].end = '-';
          data[3].attendance[k].total = '-'

        }

      }

      $rootScope.employeeData = data[3];

      if (data[4][0] != null && data[4][1] != null) {

        // change here
        $rootScope.local_income = data[4][0];
        // change here
        $rootScope.local_expenses = data[4][1];
        // change here
        $rootScope.accounting.income = Number($rootScope.local_income.amount);
        // change here
        $rootScope.accounting.expenses = Number($rootScope.local_expenses.amount);

      }

      $rootScope.activeEmployees = data[5];

    });

    $state.go('app.attendance');

  }

}


function RegisterPointsCtrl($state, AuthService, $rootScope, WebService, $http, $scope) {

  $scope.sendPoints = sendPoints;

  function sendPoints() {

    $rootScope.points.employee_id = AuthService.getUser();

    $http.post(WebService.ADDPOINTS, $rootScope.points).then(successAddPoints, $rootScope.failure);

  }

  function successAddPoints(data) {
    if (data)
      $state.go('app.register_additional', {role: '1'});
  }

}


function RequestsCtrl($ionicPopup, WebService, $rootScope, AuthService, $http, $scope) {

  $scope.disconnectEmployee = disconnectEmployee;
  $scope.approveEmployee = approveEmployee;

  $scope.doRefresh = function () {

    $rootScope.getEmployees().finally(function () {
      $scope.$broadcast('scroll.refreshComplete');
    });

  };

  function disconnectEmployee(index) {

    $http.post(WebService.UPDATECONNECTION, {

      employee_id: index,
      company_id: AuthService.getUser(),
      event: 'disconnected'

    }).then(successDisconnectEmployee, $rootScope.failure)

  }

  function successDisconnectEmployee(data) {

    if (data.status == '1') {

      $ionicPopup.alert({title: "הפעולה בוצעה בהצלחה", buttons: [{text: 'אישור'}]});
      $rootScope.getEmployees();

    } else
      $rootScope.failure();

  }

  function approveEmployee(index) {

    $http.post(WebService.UPDATECONNECTION, {

      employee_id: index,
      company_id: AuthService.getUser(),
      event: 'approved'

    }).then(successApproveEmployee, $rootScope.failure)

  }

  function successApproveEmployee(data) {

    if (data.status == '1') {

      $ionicPopup.alert({title: "הפעולה בוצעה בהצלחה", buttons: [{text: 'אישור'}]});
      $rootScope.getEmployees();

    } else
      $rootScope.failure();

  }

}

function SalariesCtrl($localStorage, $scope, $rootScope, $stateParams) {

  $scope.$on('$ionicView.enter', function () {

    if ($localStorage.role === '0') {

      angular.forEach($rootScope.employees, function (employee) {

        if (employee.employee.index == $stateParams.employee_id)
          $rootScope.employeeData = employee.employeeData;

      })

    }

  });

  $scope.doRefresh = function () {

    $rootScope.getEmployeeData($stateParams.employee_id).then(function (result) {
      $rootScope.employeeData = result;
    })
      .finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
      });

  };

  $scope.subtractMonth = subtractMonth;
  $scope.addMonth = addMonth;

  function subtractMonth() {
    $scope.today.subtract(1, 'month');
    $rootScope.getEmployeeData($stateParams.employee_id).then(function (result) {
      $rootScope.employeeData = result;
    });
  }

  function addMonth() {
    $scope.today.add(1, 'month');
    $rootScope.getEmployeeData($stateParams.employee_id).then(function (result) {
      $rootScope.employeeData = result;
    })
  }
}

function StatisticsCtrl($rootScope, $scope) {

  $scope.doRefresh = function () {

    $rootScope.getStatistics().finally(function () {
      $scope.$broadcast('scroll.refreshComplete');
    });

  };

}


