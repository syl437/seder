angular.module('starter.services', [])

  .constant('LARAVEL_HOST', 'http://mystor.co.il/seder/laravel/public/')
  .constant('EXCEL_SERVICE', 'https://biziexcel-app.herokuapp.com/api/')
  .service('AuthService', AuthService)

  .factory('WebService', WebService)

  .factory('WebServiceHttpInterceptor', WebServiceHttpInterceptor)

  .service('CheckService', CheckService)

;

function AuthService($rootScope, $http, $localStorage, $q, WebService) {

  // Public

  this.authenticate = authenticate;

  this.getUser = getUser;

  this.getMail = getMail;

  this.isAuthenticated = isAuthenticated;

  this.logout = logout;

  // Private

  function authenticate(email, password, push_id) {

    var deferred = $q.defer();

    $http.post(WebService.LOGIN, {email: email, password: password, push_id: push_id}).then(success, failure);

    function success(data) {

      // If credentials are invalid
      if (data === undefined) {
        deferred.resolve();
        return;
      }

      if (data.status == "0") {

        deferred.reject({error: 'invalidCredentials'});

        return;
      }

      // if (data.status == "1") {
      //
      //     deferred.reject({error: 'notApproved'});
      //
      //     return;
      // }

      // Else

      $localStorage.firstname = data.firstname;
      $localStorage.lastname = data.lastname;
      $localStorage.email = data.email;
      $localStorage.phone = data.phone;
      $localStorage.gender = data.gender;
      $localStorage.birthday = data.birthday;
      $localStorage.role = data.role;
      $rootScope.role = $localStorage.role;

      if (data.role == "0") {

        $localStorage.company_id = data.index;
        $rootScope.company_id = $localStorage.company_id;

        $localStorage.title = data.title;
        $localStorage.address = data.address;
        $localStorage.desc = data.desc;
        $localStorage.employee_count = data.employee_count;

      }
      else {

        $localStorage.employee_id = data.userid;
        $rootScope.connectionExists = typeof $localStorage.company_id !== 'undefined';
        $localStorage.approved = data.approved;
        $rootScope.local_employee_id = $localStorage.employee_id;
        $localStorage.company_id = data.company_id;
        $localStorage.can_add_expenses = data.can_add_expenses;
        $rootScope.can_add_expenses = $localStorage.can_add_expenses;

      }

      deferred.resolve();

    }

    function failure(error) {

      deferred.reject();

    }

    return deferred.promise;

  }

  function logout() {

    delete $localStorage.firstname;
    delete $localStorage.lastname;
    delete $localStorage.email;
    delete $localStorage.phone;
    delete $localStorage.gender;
    delete $localStorage.birthday;
    delete $localStorage.company_id;
    delete $localStorage.employee_id;
    delete $localStorage.role;
    delete $localStorage.title;
    delete $localStorage.address;
    delete $localStorage.employee_count;
    delete $localStorage.desc;
    delete $localStorage.can_add_expenses;


  }

  function isAuthenticated() {

    if ($localStorage.role == '0')
      return $localStorage.company_id !== undefined;
    else
      return $localStorage.employee_id !== undefined;
  }

  function getUser() {

    if ($localStorage.role == '0')
      return $localStorage.company_id;
    else
      return $localStorage.employee_id;
  }

  function getMail() {
    return $localStorage.email;
  }

}

function WebService(LARAVEL_HOST, EXCEL_SERVICE) {

  return {

    ACCOUNTING: LARAVEL_HOST + 'getAccounting',
    ADDACCOUNTING: LARAVEL_HOST + 'addAccounting',
    ACTIVEEMPLOYEES: LARAVEL_HOST + 'getActiveEmployees',
    ADDEXPENSES: LARAVEL_HOST + 'addExpenses',
    ADDPOINTS: LARAVEL_HOST + 'addPoints',
    ADDINCOME: LARAVEL_HOST + 'addIncome',
    ALL: LARAVEL_HOST + 'allData',
    APPROVEDEMPLOYEES: LARAVEL_HOST + 'getApprovedEmployees',
    ATTENDANCE: LARAVEL_HOST + 'GetUserWorkingHours',
    BALANCE: LARAVEL_HOST + 'getBalance',
    COMPANY: LARAVEL_HOST + 'getCompany',
    DELETEINCOME: LARAVEL_HOST + 'deleteIncome',
    DELETEEXPENSES: LARAVEL_HOST + 'deleteExpenses',
    EMPLOYEE: LARAVEL_HOST + 'getEmployee',
    EMPLOYEEALL: LARAVEL_HOST + 'employeeData',
    EMPLOYEEDATA: LARAVEL_HOST + 'getEmployeeData',
    EMPLOYEES: LARAVEL_HOST + 'getEmployees',
    EXCEL: LARAVEL_HOST + 'getExcel',
    SENDEXCEL: EXCEL_SERVICE + 'sendExcel',
    EXPENSES: LARAVEL_HOST + 'getExpenses',
    FLOW: LARAVEL_HOST + 'getFlow',
    FORGOT: LARAVEL_HOST + 'forgotPassword',
    INCOME: LARAVEL_HOST + 'getIncome',
    LIVE: LARAVEL_HOST + 'getLive',
    LOGIN: LARAVEL_HOST + 'UserLogin',
    REGISTER: LARAVEL_HOST + 'UserRegister',
    REGISTERENTERPRISE: LARAVEL_HOST + 'EnterpriseRegister',
    SENDTIME: LARAVEL_HOST + 'AddWorkingHours',
    STATISTICS: LARAVEL_HOST + 'getStatistics',
    UPDATEACCOUNTING: LARAVEL_HOST + 'updateAccounting',
    UPDATECONNECTION: LARAVEL_HOST + 'updateConnection',
    UPDATECOMPANY: LARAVEL_HOST + 'updateCompany',
    UPDATEEMPLOYEE: LARAVEL_HOST + 'updateEmployee',
    UPDATEHOURS: LARAVEL_HOST + 'updateWorkingHours',
    UPDATELIVE: LARAVEL_HOST + 'updateLive',
    UPDATEPERMISSION: LARAVEL_HOST + 'updatePermission'

  }
}


function WebServiceHttpInterceptor($injector, $httpParamSerializerJQLike, LARAVEL_HOST) {

  return {

    'request': function (config) {


      // For regular requests do nothing
      if (!config.url.startsWith(LARAVEL_HOST))
        return config;

      if (!(config.data.hideLoading === true))
        $injector.get("$ionicLoading").show();
      // For API requests transform it to applicable form

      config.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
      delete config.headers['Origin'];
      config.paramSerializer = $httpParamSerializerJQLike;
      config.data = $httpParamSerializerJQLike(config.data);

      return config;
    },

    'response': function (data) {

      // For regular requests do nothing

      if (!data.config.url.startsWith(LARAVEL_HOST))
        return data;

      // For API requests transform it to applicable form

      $injector.get("$ionicLoading").hide();
      return data.data

    },

    'responseError': function (rejection) {

      console.log(rejection);
      $injector.get("$ionicLoading").hide();

    }

  }

}


function CheckService($ionicPopup) {

  this.isAnyFieldEmpty = isAnyFieldEmpty;

  this.formatDuration = formatDuration;

  function isAnyFieldEmpty(x) {

    var keys = Object.getOwnPropertyNames(x);

    for (var key in keys) {

      if (x[keys[key]] === '' || typeof x[keys[key]] === 'undefined') {

        $ionicPopup.alert({
          title: "נא למלא את כל השדות",
          buttons: [{text: 'אישור'}]
        });

        return true;

      }

    }

    return false;

  }

  function formatDuration(duration) {

    var days = duration.days();
    var hours = duration.hours();
    var minutes = duration.minutes();

    return (days * 24 + hours) + '.' + (minutes < 10 ? '0' + minutes : minutes);

  }

}

