// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services',
  'google.places', 'ngCordova', 'angularMoment', 'ngStorage', 'angular.morris'])

  .run(function ($timeout, $q, CheckService, $http, WebService, $state, AuthService, $localStorage, $ionicLoading, $ionicPlatform, $rootScope, $ionicPopup, amMoment) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

    });

    // Push
    document.addEventListener('deviceready', function () {
      // Enable to debug issues.
      // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

      var notificationOpenedCallback = function (jsonData) {

        //alert(jsonData.notification.payload.additionalData.type);
        var type = jsonData.notification.payload.additionalData.type;

        if (type === 'approved') {

          $state.go('app.login');

        } else if (type === 'connected') {

          if (AuthService.isAuthenticated())
            $state.go('app.requests');
          else
            $state.go('app.login');

        } else if (type === 'reminder') {

          if (AuthService.isAuthenticated())
            $state.go('app.attendance');
          else
            $state.go('app.login');

        }
      };

      window.plugins.OneSignal
        .startInit("cfcad242-6937-4639-a1a7-7173d25a40a5")
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();

      window.plugins.OneSignal.getIds(function (ids) {
        $rootScope.push_id = ids.userId;
      });

      // $rootScope.updateLiveLocation(location.latitude, location.longitude);

      if (ionic.Platform.platform() !== 'ios') {
        var backgroundGeolocationConfig = {

          desiredAccuracy: 10,
          stationaryRadius: 20,
          distanceFilter: 10,
          debug: false,
          stopOnTerminate: false,
          interval: 2000,//
          startForeground: true,
          fastestInterval: 500,
          activitiesInterval: 1000,
          maxLocations: 1000000,
          notificationTitle: "Testing", // Android
          notificationText: "Testing", // Android
          startOnBoot: true

        };

        window.backgroundGeolocation.configure(
          function (location) {

            $rootScope.updateLiveLocation(location.latitude, location.longitude);

          },

          function (error) {
            console.log(error);
          },

          backgroundGeolocationConfig
        );


        backgroundGeolocation.isLocationEnabled(function (enabled) {

          if (enabled) {

            backgroundGeolocation.start(
              function () {

                // service started successfully

                // you should adjust your app UI for example change switch element to indicate

                // that service is running

              },

              function (error) {

                // Tracking has not started because of error

                // you should adjust your app UI for example change switch element to indicate

                // that service is not running

                if (error.code === 2) {

                  if (

                    window.confirm(
                      "Not authorized for location updates. Would you like to open app settings?"
                    )

                  ) {

                    backgroundGeolocation.showAppSettings();

                  }

                } else {

                  window.alert("Start failed: " + error.message);

                }

              }
            );

          } else {

            // Location services are disabled

            if (

              window.confirm(
                "Location is disabled. Would you like to open location settings?"
              )

            ) {

              backgroundGeolocation.showLocationSettings();

            }

          }

        });
      } else {
        console.log('configuring ios background geolocation');
        var bgGeo = window.BackgroundGeolocation;

        //This callback will be executed every time a geolocation is recorded in the background.
        var callbackFn = function (location, taskId) {
          $rootScope.updateLiveLocation(location.latitude, location.longitude);

          // Must signal completion of your callbackFn.
          bgGeo.finish(taskId);
        };

        // This callback will be executed if a location-error occurs.  Eg: this will be called if user disables location-services.
        var failureFn = function (errorCode) {
          console.warn('- BackgroundGeoLocation error: ', errorCode);
        }

        // Listen to location events & errors.
        bgGeo.on('location', callbackFn, failureFn);

        var backgroundGeolocationConfig = {
          desiredAccuracy: 10,
          stationaryRadius: 20,
          distanceFilter: 10,
          debug: false,
          stopOnTerminate: false,
          activityRecognitionInterval: 1000
        };

        bgGeo.configure(backgroundGeolocationConfig, function (state) {
          // This callback is executed when the plugin is ready to use.
          console.log("BackgroundGeolocation ready: ", state);
          if (!state.enabled) {
            bgGeo.start();
          }
        });
      }


    }, false);


    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

      if (toState.name === "app.login" && AuthService.isAuthenticated()) {

        event.preventDefault();

        if ($localStorage.role === '0')
          $state.go('app.balance');
        else
          $state.go('app.attendance');

      }

      $rootScope.today = moment();

    });

    amMoment.changeLocale('he');

    $rootScope.mode = typeof $localStorage.mode !== 'undefined' ? $localStorage.mode : 0;
    $rootScope.role = typeof $localStorage.role !== 'undefined' ? $localStorage.role : '';
    $rootScope.can_add_expenses = typeof $localStorage.can_add_expenses !== 'undefined' ? $localStorage.can_add_expenses : '';
    $rootScope.company_id = $localStorage.company_id;
    $rootScope.connectionExists = typeof $localStorage.company_id !== 'undefined';
    $rootScope.local_employee_id = AuthService.getUser();
    $rootScope.push_id = '';
    $rootScope.points = {
      gender: 'null',
      family_status: '0',
      young_volunteer: '0',
      children_quantity: '0',
      children_possession: '0',
      alimony: '0',
      children_invalids: '0',
      ole_hadash: '0',
      soldier: '0',
      national_service: '0',
      bachelor: '0',
      master: '0',
      doctor: '0'
    };
    $rootScope.attendance = [];
    $rootScope.today = moment();
    $rootScope.employee = {
      data: {firstname: '', lastname: '', password: '', phone: '', birthday: ''},
      salary: {amount: '', other: ''},
      point: {
        gender: 'null',
        family_status: '0',
        young_volunteer: '0',
        children_quantity: '0',
        children_possession: '0',
        alimony: '0',
        children_invalids: '0',
        ole_hadash: '0',
        soldier: '0',
        national_service: '0',
        bachelor: '0',
        master: '0',
        doctor: '0'
      }
    };
    $rootScope.flow = null;
    $rootScope.employeeData = {};
    $rootScope.accounting = {income: '', expenses: ''};
    // change here
    $rootScope.local_income = null;
    // change here
    $rootScope.local_expenses = null;
    $rootScope.balance = null;
    $rootScope.statistics = null;
    $rootScope.company = {
      firstname: '',
      lastname: '',
      password: '',
      phone: '',
      birthday: '',
      title: '',
      address: '',
      employee_count: '',
      desc: ''
    };
    $rootScope.employees = [];
    $rootScope.activeEmployees = [];
    $rootScope.permissions = [];
    $rootScope.expenses = [];
    $rootScope.income = [];

    // on load employee

    $rootScope.getAttendanceLastMonth = getAttendanceLastMonth;
    $rootScope.getAttendance = getAttendance;
    $rootScope.getEmployee = getEmployee;
    $rootScope.getFlow = getFlow;
    $rootScope.getEmployeeData = getEmployeeData;
    $rootScope.getAccounting = getAccounting;

    // update live location
    $rootScope.updateLiveLocation = updateLiveLocation;

    // on load manager

    $rootScope.getBalance = getBalance;
    $rootScope.getStatistics = getStatistics;
    $rootScope.getCompany = getCompany;
    $rootScope.getEmployees = getEmployees;
    $rootScope.getActiveEmployees = getActiveEmployees;
    $rootScope.getApprovedEmployees = getApprovedEmployees;
    $rootScope.getExpenses = getExpenses;
    $rootScope.getIncome = getIncome;

    // loading employee and manager

    $rootScope.failure = function () {

      $ionicLoading.hide();
      $ionicPopup.alert({title: "אין התחברות!", buttons: [{text: 'אישור'}]});

    };

    if ($localStorage.role === '0' && AuthService.isAuthenticated()) {       // manager

      $http.post(WebService.ALL, {company_id: AuthService.getUser()})

        .then(function (data) {

          console.log(data);

          $rootScope.balance = data[0];
          $rootScope.statistics = data[1];
          $rootScope.company = {

            firstname: data[2].firstname,
            lastname: data[2].lastname,
            phone: Number(data[2].phone),
            birthday: moment(data[2].birthday, 'DD/MM/YYYY').toDate(),
            password: '',
            title: data[2].title,
            address: data[2].address,
            employee_count: data[2].employee_count,
            desc: data[2].desc

          };
          $rootScope.employees = data[3];
          $rootScope.activeEmployees = data[4];
          $rootScope.permissions = data[5];
          $rootScope.expenses = data[6];
          $rootScope.income = data[7];

        })

        .then(function () {

          $timeout(function () {

            angular.forEach($rootScope.employees, function (employee) {

              $rootScope.getAttendance(employee.employee.index).then(function (result) {

                employee.attendance = result;

                $rootScope.getEmployee(employee.employee.index).then(function (result) {

                  employee.location = {lat: result.data.lat, lng: result.data.lng};
                  employee.profile = {
                    firstname: result.data.firstname,
                    lastname: result.data.lastname,
                    birthday: result.data.birthday,
                    phone: result.data.phone,
                    salary: result.salary.amount,
                    other_expenses: result.salary.other_expenses
                  };

                  $rootScope.getEmployeeData(employee.employee.index).then(function (result) {
                    employee.employeeData = result;
                  });

                });

              });


            })

          }, 100)
        }, function () {
          $ionicLoading.hide();
          $ionicPopup.alert({title: "אין התחברות!", buttons: [{text: 'אישור'}]});
        });

    } else if ($localStorage.role !== '0' && AuthService.isAuthenticated()) {       // employee

      $http.post(WebService.EMPLOYEEALL, {

        user: AuthService.getUser(),
        start: moment($rootScope.today).startOf('month').format('YYYY-MM-DD'),
        end: moment($rootScope.today).endOf('month').format('YYYY-MM-DD'),
        date: moment().isAfter(moment('05:59', 'HH:mm'), 'hours') ? moment().format('YYYY-MM-DD') : moment().subtract('1', 'day').format('YYYY-MM-DD'),
        company_id: $rootScope.company_id

      }).then(function (data) {

        console.log(data);

        for (var i = 0; i < data[0].length; i++) {

          if (data[0][i].end != '') {

            var checkin = moment(data[0][i].start_date + ' ' + data[0][i].start, 'YYYY-MM-DD HH:mm');
            var checkout = moment(data[0][i].end_date + ' ' + data[0][i].end, 'YYYY-MM-DD HH:mm');

            // if (checkout < checkin)
            //   checkout.add(1, 'day');

            var duration = moment.duration(checkout.diff(checkin));

            data[0][i].total = CheckService.formatDuration(duration);
            $localStorage.mode = 0;
            $rootScope.mode = $localStorage.mode;

          } else {

            data[0][i].end = '-';
            data[0][i].total = '-';
            $localStorage.mode = 1;
            $rootScope.mode = $localStorage.mode;

          }

          data[0][i].date = moment(data[0][i].date, 'YYYY-MM-DD').format('DD/MM/YYYY')

        }

        $rootScope.attendance = data[0];

        $rootScope.employee = {
          data: {
            firstname: data[1].firstname,
            lastname: data[1].lastname,
            password: '',
            phone: Number(data[1].phone),
            birthday: moment(data[1].birthday, 'DD/MM/YYYY').toDate(),
            lat: data[1].location_lat,
            lng: data[1].location_lng
          },
          salary: {
            amount: parseFloat(data[1].salary.amount),
            other_expenses: parseFloat(data[1].salary.other_expenses)
          },
          point: {
            gender: String(data[1].point.gender),
            family_status: data[1].point.family_status,
            young_volunteer: data[1].point.young_volunteer,
            children_quantity: data[1].point.children_quantity,
            children_possession: data[1].point.children_possession,
            alimony: data[1].point.alimony,
            children_invalids: data[1].point.children_invalids,
            ole_hadash: data[1].point.ole_hadash,
            soldier: data[1].point.soldier,
            national_service: data[1].point.national_service,
            bachelor: data[1].point.bachelor,
            master: data[1].point.master,
            doctor: data[1].point.doctor
          }
        };

        $rootScope.flow = data[2];

        for (var k = 0; k < data[3].attendance.length; k++) {

          data[3].attendance[k].date = moment(data[3].attendance[k].date, 'YYYY-MM-DD').format('DD/MM/YYYY');

          if (data[3].attendance[k].end != '') {

            var checkin = moment(data[3].attendance[k].start_date + ' ' + data[3].attendance[k].start, 'YYYY-MM-DD HH:mm');
            var checkout = moment(data[3].attendance[k].end_date + ' ' + data[3].attendance[k].end, 'YYYY-MM-DD HH:mm');

            // if (checkout < checkin)
            //   checkout.add(1, 'day');

            var duration = moment.duration(checkout.diff(checkin));
            var arr = CheckService.formatDuration(duration).split('.');

            data[3].attendance[k].total = parseFloat(arr[0] * data[3].salary.amount + ((arr[1] / 6) * 10) * (data[3].salary.amount / 100));

          } else {

            data[3].attendance[k].end = '-';
            data[3].attendance[k].total = '-'

          }

        }

        $rootScope.employeeData = data[3];

        if (data[4][0] != null && data[4][1] != null) {

          // change here
          $rootScope.local_income = data[4][0];
          // change here
          $rootScope.local_expenses = data[4][1];
          // change here
          $rootScope.accounting.income = Number($rootScope.local_income.amount);
          // change here
          $rootScope.accounting.expenses = Number($rootScope.local_expenses.amount);

        }

        $rootScope.activeEmployees = data[5];

      });

    }

    // get attandance last month
    function getAttendanceLastMonth(x) {

      return $http.post(WebService.ATTENDANCE, {

        user: x,
        start: moment(new Date()).startOf('month').format('YYYY-MM-DD'),
        end: moment(new Date()).endOf('month').format('YYYY-MM-DD')

      }).then(successAttendanceLastMonth, $rootScope.failure)

    }

    function successAttendanceLastMonth(data) {
      if (data.length === 0) {
        return new Array();
      }

      var total_bonus = 0;
      var total_hours = 0;
      var total_money = 0
      var total_full_money = 0;

      for (var i = 0; i < data.length; i++) {

        if (data[i].end != '') {

          var checkin = moment(data[i].start_date + ' ' + data[i].start, 'YYYY-MM-DD HH:mm');
          var checkout = moment(data[i].end_date + ' ' + data[i].end, 'YYYY-MM-DD HH:mm');

          // if (checkout < checkin)
          //   checkout.add(1, 'day');

          var duration = moment.duration(checkout.diff(checkin));

          data[i].total = CheckService.formatDuration(duration);
          $localStorage.mode = 0;
          $rootScope.mode = $localStorage.mode;

          total_hours += data[i].total;


        } else {

          data[i].end = '-';
          data[i].total = '-';
          $localStorage.mode = 1;
          $rootScope.mode = $localStorage.mode;

        }

        data[i].date = moment(data[i].date, 'YYYY-MM-DD').format('DD/MM/YYYY')

      }

      $rootScope.employeesRows[data[0].employee_id].sumHours = total_hours;
      $rootScope.employeesRows[data[0].employee_id].sumSal = total_hours * $rootScope.employeesRows[data[0].employee_id].salPerHour;
      $rootScope.employeesRows[data[0].employee_id].sumMoney = $rootScope.employeesRows[data[0].employee_id].sumSal + (data.length * $rootScope.employeesRows[data[0].employee_id].bonus);
      $rootScope.employeesRows[data[0].employee_id].sumDays = data.length;

      return $rootScope.employeesRows;
    }

    // get attendance

    function getAttendance(x) {

      return $http.post(WebService.ATTENDANCE, {

        user: x,
        start: moment($rootScope.today).startOf('month').format('YYYY-MM-DD'),
        end: moment($rootScope.today).endOf('month').format('YYYY-MM-DD')

      }).then(successAttendance, $rootScope.failure)

    }

    function successAttendance(data) {

      for (var i = 0; i < data.length; i++) {

        if (data[i].end != '') {

          var checkin = moment(data[i].start_date + ' ' + data[i].start, 'YYYY-MM-DD HH:mm');
          var checkout = moment(data[i].end_date + ' ' + data[i].end, 'YYYY-MM-DD HH:mm');

          // if (checkout < checkin)
          //   checkout.add(1, 'day');

          var duration = moment.duration(checkout.diff(checkin));

          data[i].total = CheckService.formatDuration(duration);
          $localStorage.mode = 0;
          $rootScope.mode = $localStorage.mode;

        } else {

          data[i].end = '-';
          data[i].total = '-';
          $localStorage.mode = 1;
          $rootScope.mode = $localStorage.mode;

        }

        data[i].date = moment(data[i].date, 'YYYY-MM-DD').format('DD/MM/YYYY')

      }

      $rootScope.attendance = data;
      console.log('Attendance', $rootScope.attendance);
      return data;
    }

    // employee profile


    function getEmployee(x) {

      return $http.post(WebService.EMPLOYEE, {user: x}).then(successEmployee, $rootScope.failure)

    }

    function successEmployee(data) {

      $rootScope.employee = {
        data: {
          firstname: data.firstname,
          lastname: data.lastname,
          password: '',
          phone: Number(data.phone),
          birthday: moment(data.birthday, 'DD/MM/YYYY').toDate(),
          lat: data.location_lat,
          lng: data.location_lng
        },
        salary: {
          amount: parseFloat(data.salary.amount),
          other_expenses: parseFloat(data.salary.other_expenses)
        },
        point: {
          gender: data.point.gender,
          family_status: data.point.family_status,
          young_volunteer: data.point.young_volunteer,
          children_quantity: data.point.children_quantity,
          children_possession: data.point.children_possession,
          alimony: data.point.alimony,
          children_invalids: data.point.children_invalids,
          ole_hadash: data.point.ole_hadash,
          soldier: data.point.soldier,
          national_service: data.point.national_service,
          bachelor: data.point.bachelor,
          master: data.point.master,
          doctor: data.point.doctor
        }
      };

      console.log("Profile", $rootScope.employee);
      return $rootScope.employee;
    }

    // employee flow

    function getFlow() {

      return $http.post(WebService.FLOW, {user: AuthService.getUser()}).then(function (data) {

        console.log("Flow", data);
        $rootScope.flow = data;

      }, $rootScope.failure);

    }

    // employee salary

    function getEmployeeData(x) {

      return $http.post(WebService.EMPLOYEEDATA, {

        user: x,
        start: moment($rootScope.today).startOf('month').format('YYYY-MM-DD'),
        end: moment($rootScope.today).endOf('month').format('YYYY-MM-DD')

      }).then(successEmployeeData, $rootScope.failure);

    }

    function successEmployeeData(data) {

      for (var i = 0; i < data.attendance.length; i++) {

        data.attendance[i].date = moment(data.attendance[i].date, 'YYYY-MM-DD').format('DD/MM/YYYY');

        if (data.attendance[i].end != '') {

          var checkin = moment(data.attendance[i].start_date + ' ' + data.attendance[i].start, 'YYYY-MM-DD HH:mm');
          var checkout = moment(data.attendance[i].end_date + ' ' +data.attendance[i].end, 'YYYY-MM-DD HH:mm');

          // if (checkout < checkin)
          //   checkout.add(1, 'day');

          var duration = moment.duration(checkout.diff(checkin));
          var arr = CheckService.formatDuration(duration).split('.');

          data.attendance[i].total = parseFloat(arr[0] * data.salary.amount + ((arr[1] / 6) * 10) * (data.salary.amount / 100));

        } else {

          data.attendance[i].end = '-';
          data.attendance[i].total = '-'

        }

      }

      $rootScope.employeeData = data;
      console.log('Data', $rootScope.employeeData);
      return data;

    }

    // employee accounting


    function getAccounting() {

      return $http.post(WebService.ACCOUNTING, {

        user: AuthService.getUser(),
        date: moment().isAfter(moment('05:59', 'HH:mm'), 'hours') ? moment().format('YYYY-MM-DD') : moment().subtract('1', 'day').format('YYYY-MM-DD')

      }).then(successGetAccounting, $rootScope.failure)

    }

    function successGetAccounting(data) {

      console.log("Accounting", data);
      if (data[0] != null && data[1] != null) {

        $rootScope.local_income = data[0];
        // change here
        $rootScope.local_expenses = data[1];
        // change here
        $rootScope.accounting.income = Number($rootScope.local_income.amount);
        // change here
        $rootScope.accounting.expenses = Number($rootScope.local_expenses.amount);

      }
    }

    // manager balance

    function getBalance() {

      return $http.post(WebService.BALANCE, {company_id: AuthService.getUser()}).then(function (data) {

        console.log("Balance", data);
        $rootScope.balance = data;

      }, $rootScope.failure)

    }

    // manager statistics

    function getStatistics() {

      return $http.post(WebService.STATISTICS, {company_id: AuthService.getUser()}).then(function (data) {

        console.log("Statistics", data);
        $rootScope.statistics = data;

      }, $rootScope.failure);

    }

    // get manager / company profile

    function getCompany() {

      return $http.post(WebService.COMPANY, {company_id: AuthService.getUser()}).then(successCompany, $rootScope.failure)

    }

    function successCompany(data) {

      $rootScope.company = {

        firstname: data.firstname,
        lastname: data.lastname,
        phone: Number(data.phone),
        birthday: moment(data.birthday, 'DD/MM/YYYY').toDate(),
        password: '',
        title: data.title,
        address: data.address,
        employee_count: data.employee_count,
        desc: data.desc

      };
      console.log("Company", $rootScope.company);
    }

    // get employees for requests page

    function getEmployees() {

      return $http.post(WebService.EMPLOYEES, {company_id: AuthService.getUser()}).then(function (data) {

        $rootScope.employees = data;
        console.log('Requests/employees', $rootScope.employees);
        return data;

      }, $rootScope.failure);

    }

    function getActiveEmployees(company_id, hideLoadingBar) {

      return $http.post(WebService.ACTIVEEMPLOYEES, {
        company_id: company_id,
        hideLoading: hideLoadingBar
      }).then(function (data) {

        $rootScope.activeEmployees = data;
        console.log('Active employees', $rootScope.activeEmployees);

      }, $rootScope.failure);

    }

    function getApprovedEmployees() {

      return $http.post(WebService.APPROVEDEMPLOYEES, {company_id: AuthService.getUser()}).then(function (data) {

        $rootScope.permissions = data;
        console.log('Permissions/employees', $rootScope.permissions);

      }, $rootScope.failure);

    }

    function getExpenses(hideLoadingBar) {

      return $http.post(WebService.EXPENSES, {
        company_id: AuthService.getUser(),
        hideLoading: hideLoadingBar
      }).then(function (data) {

        $rootScope.expenses = data;
        console.log('expenses', $rootScope.expenses);

      }, $rootScope.failure)

    }

    function getIncome(hideLoadingBar) {

      return $http.post(WebService.INCOME, {
        company_id: AuthService.getUser(),
        hideLoading: hideLoadingBar
      }).then(function (data) {

        $rootScope.income = data;
        console.log('Income', $rootScope.income);

      }, $rootScope.failure)

    }

    function updateLiveLocation(lat, lng) {

      $http.post(WebService.UPDATELIVE, {user: AuthService.getUser(), lat: lat, lng: lng}).then(function (data) {

        console.log(data);

      }, $rootScope.failure)

    }

  })

  .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })

      .state('app.accounting', {
        url: '/accounting',
        views: {
          'menuContent': {
            templateUrl: 'templates/accounting.html',
            controller: 'AccountingCtrl'
          }
        }
      })

      .state('app.attendance', {
        url: '/attendance',
        views: {
          'menuContent': {
            templateUrl: 'templates/attendance.html',
            controller: 'AttendanceCtrl'
          }
        }
      })


      .state('app.balance', {
        url: '/balance',
        views: {
          'menuContent': {
            templateUrl: 'templates/balance.html',
            controller: 'BalanceCtrl'
          }
        }
      })

      .state('app.conditions', {
        url: '/conditions',
        views: {
          'menuContent': {
            templateUrl: 'templates/conditions.html'
          }
        }
      })

      .state('app.connection', {
        url: '/connection',
        views: {
          'menuContent': {
            templateUrl: 'templates/connection.html',
            controller: 'ConnectionCtrl'
          }
        }
      })

      .state('app.employees', {
        url: '/employees',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/employees.html',
            controller: 'EmployeesCtrl'
          }
        }
      })

      .state('app.employees.employee', {
        url: '/:employee_id',
        cache: false,
        abstract: true
      })

      .state('app.employees.employee.hours', {
        url: '/hours',
        cache: false,
        views: {
          'menuContent@app': {
            templateUrl: 'templates/hours.html',
            controller: 'HoursCtrl'
          }
        }
      })

      .state('app.employees.employee.location', {
        url: '/location',
        cache: false,
        views: {
          'menuContent@app': {
            templateUrl: 'templates/location.html',
            controller: 'LocationCtrl'
          }
        }
      })

      .state('app.employees.employee.profile_for_manager', {
        url: '/profile_for_manager',
        cache: false,
        views: {
          'menuContent@app': {
            templateUrl: 'templates/profile_for_manager.html',
            controller: 'ProfileForManagerCtrl'
          }
        }
      })

      .state('app.employees.employee.salaries', {
        url: '/salaries',
        cache: false,
        views: {
          'menuContent@app': {
            templateUrl: 'templates/salaries.html',
            controller: 'SalariesCtrl'
          }
        }
      })

      .state('app.expenses', {
        url: '/expenses',
        views: {
          'menuContent': {
            templateUrl: 'templates/expenses.html',
            controller: 'ExpensesCtrl'
          }
        }
      })

      .state('app.flow', {
        url: '/flow',
        views: {
          'menuContent': {
            templateUrl: 'templates/flow.html',
            controller: 'FlowCtrl'
          }
        }
      })

      .state('app.map', {
        url: '/map/:employee_id/:lat/:lng',
        views: {
          'menuContent': {
            templateUrl: 'templates/map.html',
            controller: 'MapCtrl'
          }
        }
      })

      .state('app.income', {
        url: '/income',
        views: {
          'menuContent': {
            templateUrl: 'templates/income.html',
            controller: 'IncomeCtrl'
          }
        }
      })

      .state('app.login', {
        url: '/login',
        views: {
          'menuContent': {
            templateUrl: 'templates/login.html',
            controller: 'LoginCtrl'
          }
        }
      })

      .state('app.permissions', {
        url: '/permissions',
        views: {
          'menuContent': {
            templateUrl: 'templates/permissions.html',
            controller: 'PermissionsCtrl'
          }
        }
      })

      .state('app.profile_company', {
        url: '/profile_company',
        views: {
          'menuContent': {
            templateUrl: 'templates/profile_company.html',
            controller: 'ProfileCompanyCtrl'
          }
        }
      })

      .state('app.profile_employee', {
        url: '/profile_employee',
        views: {
          'menuContent': {
            templateUrl: 'templates/profile_employee.html',
            controller: 'ProfileEmployeeCtrl'
          }
        }
      })

      .state('app.register', {
        url: '/register/:role',
        views: {
          'menuContent': {
            templateUrl: 'templates/register.html',
            controller: 'RegisterCtrl'
          }
        }
      })

      .state('app.register_additional', {
        url: '/register_additional/:role',
        views: {
          'menuContent': {
            templateUrl: 'templates/register_additional.html',
            controller: 'RegisterAdditionalCtrl'
          }
        }
      })

      .state('app.register_points1', {
        url: '/register_points1/:role',
        views: {
          'menuContent': {
            templateUrl: 'templates/register_points1.html',
            controller: 'RegisterPointsCtrl'
          }
        }
      })


      .state('app.register_points2', {
        url: '/register_points2/:role',
        views: {
          'menuContent': {
            templateUrl: 'templates/register_points2.html',
            controller: 'RegisterPointsCtrl'
          }
        }
      })

      .state('app.requests', {
        url: '/requests',
        views: {
          'menuContent': {
            templateUrl: 'templates/requests.html',
            controller: 'RequestsCtrl'
          }
        }
      })

      .state('app.statistics', {
        url: '/statistics',
        views: {
          'menuContent': {
            templateUrl: 'templates/statistics.html',
            controller: 'StatisticsCtrl'
          }
        }
      })

      .state('app.terms', {
        url: '/terms',
        views: {
          'menuContent': {
            templateUrl: 'templates/terms.html'
          }
        }
      })

    ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/login');

    $httpProvider.interceptors.push('WebServiceHttpInterceptor');

  });
